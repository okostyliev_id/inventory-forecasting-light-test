<#include "/common.ftl">
WITH
-- historical data filtered by country, platform, placement
hd as (
    select
            s.d, s.total_impressions, s.placement_id
    from
            iow_rollups.iponweb_daily_stats s
    where
            -- go 84 days back
            datediff(NOW(), s.d) <= 84
            <#if filterByCountries>
            -- country filter
            and s.country_id in (${join(country_ids)})
            </#if>
            <#if filterByPlatforms>
            -- platform filter
            and s.platform_id in (${join(platform_ids)})
            </#if>
            <#if filterByPlacementIds>
            -- placement filter
            and s.placement_id in (${join(placement_ids)})
            </#if>
)
<#assign filterByZoneOrSiteIds = !filterByPlacementIds && (filterByZoneIds || filterBySiteIds)>
<#if filterByZoneOrSiteIds>,
-- filter placements by site or zone
plcmnt as (
    select
            distinct pl.placement_id
    from
            metadata_db.placement pl
    where
            pl.publisher_id = ${publisher_id} and
            <#if filterByZoneIds>
            pl.zone_id in (${join(zone_ids)})
            <#else>
            pl.site_id in (${join(site_ids)})
            </#if>
)
</#if>
<#if filterBySizes>,
-- filter placements by sizes
plsize as (
    select
            distinct pls.placement_id
    from
            metadata_db.placement_size pls
    where
            pls.size_id in (${join(size_ids)})
)
</#if>
-- combine all of the filters together to get the dataset
select
        sum(hd.total_impressions) as total_impressions, hd.d as 'day'
from
        hd
        <#if filterByZoneOrSiteIds>
        inner join plcmnt on hd.placement_id = plcmnt.placement_id
        </#if>
        <#if filterBySizes>
        inner join plsize on hd.placement_id = plsize.placement_id
        </#if>
group by
        hd.d
order by
        hd.d
;