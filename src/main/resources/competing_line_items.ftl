<#include "/common.ftl">
WITH
<#assign filterPlacementsByIds = filterByPlacementIds || filterByZoneIds || filterBySiteIds>
<#if filterPlacementsByIds>
-- filter placements by id, site or zone
plcmnt as (
        select
                distinct pl.placement_id
        from
                metadata_db.placement pl
        where
                pl.publisher_id = ${publisher_id} and
                <#if filterByPlacementIds>
                pl.placement_id in (${join(placement_ids)})
                <#elseif filterByZoneIds>
                pl.zone_id in (${join(zone_ids)})
                <#else>
                pl.site_id in (${join(site_ids)})
                </#if>
),
</#if>
<#if filterByPlatforms>
-- filter placements by platform
plplat as (
        select
                distinct pl.placement_id
        from
                metadata_db.placement pl
                inner join metadata_db.site s
                        on pl.publisher_id = ${publisher_id}
                        and pl.site_id = s.site_id
                        and s.platform_id in (${join(platform_ids)})

),
</#if>
<#if filterBySizes>
-- filter placements by sizes
plsize as (
        select
                distinct pls.placement_id
        from
                metadata_db.placement_size pls
        where
                pls.size_id in (${join(size_ids)})
),
</#if>
<#assign filterByPlacements = filterPlacementsByIds || filterByPlatforms || filterBySizes>
<#if filterByPlacements>
-- combine the above filters
pl as (
        select
                <#if filterPlacementsByIds>plcmnt<#elseif filterByPlatforms>plplat<#else>plsize</#if>.placement_id
        from
                <#assign toJoin = false>
                <#if filterPlacementsByIds>
                plcmnt
                <#assign toJoin = true>
                </#if>
                <#if filterByPlatforms>
                <#if toJoin>inner join </#if>plplat<#if toJoin> on plcmnt.placement_id = plplat.placement_id</#if>
                <#assign toJoin = true>
                </#if>
                <#if filterBySizes>
                <#if toJoin>inner join </#if>plsize<#if toJoin> on plcmnt.placement_id = plsize.placement_id</#if>
                </#if>
),
-- map placements to line items
lip as (
        select
                distinct lip.line_item_id
        from
                pl
                inner join metadata_db.line_item_placement lip
                        on pl.placement_id = lip.placement_id
                        and lip.exclude = 0
),
</#if>
<#if filterByCountries>
-- filter line items by countries
lic as (
        select
                distinct lic.line_item_id
        from
                metadata_db.line_item_country lic
        where
                lic.country_id in (${join(country_ids)})
                and lic.exclude = 0
),
</#if>
-- combine all of the filters together to get competing line items
li as (
        select
                li.*
        from
                metadata_db.line_item li
                <#if filterByPlacements>
                inner join lip on li.line_item_id = lip.line_item_id
                </#if>
                <#if filterByCountries>
                inner join lic on li.line_item_id = lic.line_item_id
                </#if>
        where
                li.guaranteed = 1
                and li.status_id = 1
                and li.publisher_id = ${publisher_id}
                -- extra check, guaranteed line items must normally have the start and end times specified
                and (li.start_time is not null and li.end_time is not null)
                -- that overlaps in time with current line item
                and (li.start_time <= '${end_time?date} 23:59:59' and li.end_time >= '${start_time?date} 00:00:00')
)