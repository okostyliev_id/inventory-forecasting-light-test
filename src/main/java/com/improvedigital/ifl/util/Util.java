package com.improvedigital.ifl.util;

/**
 *
 */
public class Util {

	public static void checkArgument(boolean condition, String message) {
		if (!condition) {
			throw new IllegalArgumentException(message);
		}
	}

	public static void assertNull(Object object) {
		checkArgument(object == null, "Expected null, got " + object);
	}

	public static void assertNotNull(Object object) {
		checkArgument(object != null, "Expected object, got null");
	}
}
