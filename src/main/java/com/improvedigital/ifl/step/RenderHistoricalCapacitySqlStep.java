package com.improvedigital.ifl.step;

import com.improvedigital.ifl.sql.SqlQueryRenderer;
import freemarker.template.TemplateException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClass;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassData;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassMeta;

import java.io.IOException;
import java.util.Map;

/**
 *
 */
public class RenderHistoricalCapacitySqlStep extends AbstractRenderSqlQueryStep {

	public RenderHistoricalCapacitySqlStep(UserDefinedJavaClass parent,
			UserDefinedJavaClassMeta meta,
			UserDefinedJavaClassData data) throws KettleStepException {
		super(parent, meta, data);
	}

	@Override
	protected String renderSql(SqlQueryRenderer renderer, Map<String, Object> parameters) throws IOException,
			TemplateException {
		return renderer.renderHistoricalCapacitySql(parameters);
	}

}
