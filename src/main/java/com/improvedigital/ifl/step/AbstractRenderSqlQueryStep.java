package com.improvedigital.ifl.step;

import com.improvedigital.ifl.sql.SqlQueryRenderer;
import com.improvedigital.ifl.util.Util;
import freemarker.template.TemplateException;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.userdefinedjavaclass.FieldHelper;
import org.pentaho.di.trans.steps.userdefinedjavaclass.TransformClassBase;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClass;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassData;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassMeta;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
abstract class AbstractRenderSqlQueryStep extends TransformClassBase {

	AbstractRenderSqlQueryStep(UserDefinedJavaClass parent,
			UserDefinedJavaClassMeta meta,
			UserDefinedJavaClassData data) throws KettleStepException {
		super(parent, meta, data);
	}

	@Override
	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
		String outputFieldName = processOutputMeta();
		Object[] inputRow = fetchInputRow();
		String output = renderSqlStatement(inputRow);
		createOutputRow(outputFieldName, output);
		return signalOutput();
	}

	protected abstract String renderSql(SqlQueryRenderer renderer, Map<String, Object> parameters) throws IOException,
			TemplateException;

	private boolean signalOutput() {
		setOutputDone();
		return false;
	}

	private void createOutputRow(String outputFieldName, String output) throws KettleStepException {
		Object[] outputRow = createOutputRow(null, data.outputRowMeta.size());
		get(Fields.Out, outputFieldName).setValue(outputRow, output);

		putRow(data.outputRowMeta, outputRow);
	}

	private String renderSqlStatement(Object[] inputData) throws KettleException {
		try {
			return renderSql(SqlQueryRenderer.getInstance(), getParameters(inputData));
		} catch (TemplateException | IOException | RuntimeException e) {
			throw new KettleException(e);
		}
	}

	private Object[] fetchInputRow() throws KettleException {
		Object[] inputRow = getRow();
		Util.assertNotNull(inputRow);
		Util.checkArgument(getRow() == null, "Expected only 1 input row, got more");

		return inputRow;
	}

	private String processOutputMeta() {
		String[] outputFieldNames = data.outputRowMeta.getFieldNames();
		Util.checkArgument(outputFieldNames.length == 1, "Expected only 1 output field");
		return outputFieldNames[0];
	}

	private Map<String, Object> getParameters(Object[] input) throws KettleValueException, KettleStepException {
		try {
			Map<String, Object> parameters = new HashMap<>();
			for (ValueMetaInterface valueMeta : data.inputRowMeta.getValueMetaList()) {
					parameters.put(valueMeta.getName(), getValue(input, valueMeta));
			}
			return parameters;
		} catch (ParseException e) {
			throw new KettleValueException(e);
		}
	}

	private Object getValue(Object[] input, ValueMetaInterface valueMeta) throws KettleValueException,
			KettleStepException, ParseException {
		FieldHelper inputField = get(Fields.In, valueMeta.getName());
		switch (valueMeta.getType()) {
			case ValueMetaInterface.TYPE_STRING:
				return toIntegers(inputField.getString(input));
			case ValueMetaInterface.TYPE_INTEGER:
				return inputField.getInteger(input);
			case ValueMetaInterface.TYPE_DATE:
				return new SimpleDateFormat("yyyy-MM-dd").parse(inputField.getString(input));
			default:
				throw new UnsupportedOperationException("Unsupported value type: " + valueMeta);
		}
	}

	private int[] toIntegers(String value) throws KettleValueException, KettleStepException {
		String input = value.trim();
		if (input.isEmpty()) {
			return new int[0];
		}

		String[] source = input.split(",");
		int[] result = new int[source.length];

		for (int i = 0; i < source.length; i++) {
			result[i] = Integer.valueOf(source[i].trim());
		}

		return result;
	}
}
