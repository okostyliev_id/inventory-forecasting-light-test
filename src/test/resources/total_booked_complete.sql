WITH
-- filter placements by id, site or zone
plcmnt as (
        select
                distinct pl.placement_id
        from
                metadata_db.placement pl
        where
                pl.publisher_id = 64 and
                pl.placement_id in (1288,1292,1309,1286,1295)
),
-- filter placements by platform
plplat as (
        select
                distinct pl.placement_id
        from
                metadata_db.placement pl
                inner join metadata_db.site s
                        on pl.publisher_id = 64
                        and pl.site_id = s.site_id
                        and s.platform_id in (1)

),
-- filter placements by sizes
plsize as (
        select
                distinct pls.placement_id
        from
                metadata_db.placement_size pls
        where
                pls.size_id in (13,18,221)
),
-- combine the above filters
pl as (
        select
                plcmnt.placement_id
        from
                plcmnt
                inner join plplat on plcmnt.placement_id = plplat.placement_id
                inner join plsize on plcmnt.placement_id = plsize.placement_id
),
-- map placements to line items
lip as (
        select
                distinct lip.line_item_id
        from
                pl
                inner join metadata_db.line_item_placement lip
                        on pl.placement_id = lip.placement_id
                        and lip.exclude = 0
),
-- filter line items by countries
lic as (
        select
                distinct lic.line_item_id
        from
                metadata_db.line_item_country lic
        where
                lic.country_id in (2,221)
                and lic.exclude = 0
),
-- combine all of the filters together to get competing line items
li as (
        select
                li.*
        from
                metadata_db.line_item li
                inner join lip on li.line_item_id = lip.line_item_id
                inner join lic on li.line_item_id = lic.line_item_id
        where
                li.guaranteed = 1
                and li.status_id = 1
                and li.publisher_id = 64
                -- extra check, guaranteed line items must normally have the start and end times specified
                and (li.start_time is not null and li.end_time is not null)
                -- that overlaps in time with current line item
                and (li.start_time <= '2012-06-28 23:59:59' and li.end_time >= '2012-06-20 00:00:00')
),
-- get the necessary information for competing line items
libudget as (
        SELECT
                li.line_item_id,
                li.budget,
                li.budget_is_daily,
                li.impression_cap,
                li.impression_cap_is_daily,
                datediff(li.end_time, li.start_time) + 1 as 'duration',
                datediff(
                      -- smallest end_time of the 2 line items
                      least(li.end_time, '2012-06-28'),
                      -- biggest start_time of the 2 line items
                      greatest(li.start_time, '2012-06-20')
                ) + 1 as 'days_overlap',
                lib.cpm_bid
        FROM
                li
                inner join metadata_db.line_item_bid lib on
                      li.line_item_id = lib.line_item_id
                      -- take the last known cpm
                      and lib.end_time is null
                      -- only cpm-based line items (holds for guaranteed anyway)
                      and lib.pricing_model_id = 1
),
-- calculate impression_cap per line item
liimpressions as (
        select
                libudget.line_item_id,
                CASE
                        WHEN libudget.budget IS NOT NULL THEN libudget.budget / libudget.cpm_bid * 1000
                        ELSE libudget.impression_cap
                END as 'impression_cap',
                CASE
                        WHEN libudget.budget IS NOT NULL THEN libudget.budget_is_daily
                        ELSE libudget.impression_cap_is_daily
                END as 'is_daily',
                libudget.duration,
                libudget.days_overlap
        from
                libudget
),
-- calculate the amount of impressions that overlap with the current line item
lioverlap as (
        SELECT
                liimpressions.line_item_id,
                liimpressions.impression_cap * liimpressions.days_overlap * (CASE liimpressions.is_daily WHEN 1 THEN 1 ELSE 1 / liimpressions.duration END) as 'impressions_overlap'
        FROM
                liimpressions
)
select
        cast(sum(lioverlap.impressions_overlap) as int) as 'total_booked'
from
        lioverlap
;