WITH
-- historical data filtered by country, platform, placement
hd as (
    select
            s.d, s.total_impressions, s.placement_id
    from
            iow_rollups.iponweb_daily_stats s
    where
            -- go 84 days back
            datediff(NOW(), s.d) <= 84
            -- country filter
            and s.country_id in (16,57,9)
            -- platform filter
            and s.platform_id in (1)
            -- placement filter
            and s.placement_id in (5687,235545,835972)
)
,
-- filter placements by sizes
plsize as (
    select
            distinct pls.placement_id
    from
            metadata_db.placement_size pls
    where
            pls.size_id in (4,171,637)
)
-- combine all of the filters together to get the dataset
select
        sum(hd.total_impressions) as total_impressions, hd.d as 'day'
from
        hd
        inner join plsize on hd.placement_id = plsize.placement_id
group by
        hd.d
order by
        hd.d
;