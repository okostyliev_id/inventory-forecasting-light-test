WITH
-- historical data filtered by country, platform, placement
hd as (
    select
            s.d, s.total_impressions, s.placement_id
    from
            iow_rollups.iponweb_daily_stats s
    where
            -- go 30 days back
            s.d >= date_sub(NOW(), 29)
            -- country filter
            and s.country_id in (3)
            -- platform filter
            and s.platform_id in (1)
)
,
-- filter placements by site or zone
plcmnt as (
    select
            distinct pl.placement_id
    from
            metadata_db.placement pl
    where
            pl.publisher_id = 335
            -- and pl.zone_id in (144871,221409,300539)
)
/*,
-- filter placements by sizes
plsize as (
    select
            distinct pls.placement_id
    from
            metadata_db.placement_size pls
    where
            pls.size_id in (171)
)*/
-- combine all of the filters together to get the dataset
select
        sum(hd.total_impressions) as total_impressions, hd.d as 'day'
from
        hd
        inner join plcmnt on hd.placement_id = plcmnt.placement_id
--        inner join plsize on hd.placement_id = plsize.placement_id
group by
        hd.d
order by
        hd.d;
