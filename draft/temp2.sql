WITH
-- historical data filtered by country, platform, placement
hd as (
    select
            s.d, s.total_impressions, s.placement_id
    from
            iow_rollups.iponweb_daily_stats s
    where
            -- go 28 days back
            s.d >= date_sub(NOW(), 29)
            -- country filter
            and s.country_id in (2,221)
            -- platform filter
            and s.platform_id in (1)
            -- placement filter
            and s.placement_id in (1288,1292,1309,1286,1295)
)
,
-- filter placements by sizes
plsize as (
    select
            distinct pls.placement_id
    from
            metadata_db.placement_size pls
    where
            pls.size_id in (13,18,221)
)
-- combine all of the filters together to get the dataset
select
        sum(hd.total_impressions) as total_impressions, hd.d as 'day'
from
        hd
        inner join plsize on hd.placement_id = plsize.placement_id
group by
        hd.d
order by
        hd.d
;
