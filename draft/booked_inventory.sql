SELECT
        sum(sub3.impression_overlap) as 'total_booked'
FROM (
        SELECT
                sub2.id,
                sub2.impression_cap * sub2.days_overlap * (CASE sub2.is_daily WHEN 1 THEN 1 ELSE 1 / sub2.duration END) as 'impression_overlap'
        FROM (
                SELECT
                        sub1.id,
                        CASE
                                WHEN sub1.budget IS NOT NULL THEN sub1.budget / sub1.cpm_bid * 1000
                                ELSE sub1.impression_cap
                        END as 'impression_cap',
                        CASE
                                WHEN sub1.budget IS NOT NULL THEN sub1.budget_is_daily
                                ELSE sub1.impression_cap_is_daily
                        END as 'is_daily',
                        sub1.duration,
                        sub1.days_overlap
                 FROM (
                        SELECT
                                li.id,
                                li.budget,
                                li.budget_is_daily,
                                li.cpm_bid,
                                li.impression_cap,
                                li.impression_cap_is_daily,
                                datediff(li.end_time, li.start_time) + 1 as 'duration',
                                datediff(
                                      -- smallest end_time of the 2 line items
                                      least(li.end_time, li.current_end_time),
                                      -- biggest start_time of the 2 line items
                                      greatest(li.start_time, li.current_start_time)
                                ) + 1 as 'days_overlap'
                        FROM (
                                SELECT
                                        li.*,
                                        lib.cpm_bid,
                                        timestamp('2011-09-12 10:14:56') as current_start_time,
                                        timestamp('2012-08-12 10:14:56') as current_end_time
                                FROM
                                        360yield.line_item li
                                        inner join 360yield.line_item_bid lib on
                                              li.id = lib.line_item_id
                                              -- take the last known cpm
                                              and lib.end_time is null
                                              -- only cpm-based line items (holds for guaranteed anyway)
                                              and lib.pricing_model_id = 1
                                WHERE
                                        li.guaranteed = 1
                                        -- active
                                        and li.status_id = 1
                                        -- HERE NEEDS TO GO TARGETING CRITERIA FILTERING
                                        -- ...
                        ) li
                        WHERE
                                -- extra check, guaranteed line items must normally have the start and end times specified
                                (li.start_time is not null and li.end_time is not null)
                                -- that overlaps in time with current line item
                                and (li.start_time < li.current_end_time and li.end_time > li.current_start_time)
                ) sub1
        ) sub2
) sub3
