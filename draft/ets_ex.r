library( forecast )
library( ggplot2 )

data = read.csv("/Users/oleksii/dev/inventory-forecast-light/draft/data.csv", sep = '\t')
imps = ts(data['total_impressions'], frequency = 7)
model = ets(imps, model = "ZAA")
# model = auto.arima(imps)
prediction = forecast(model, h = 7 * 12)

pdf("/Users/oleksii/dev/inventory-forecast-light/draft/data.pdf")
plot(prediction)
dev.off()