library(forecast)

imps = ts(data['total_impressions'], frequency = 7)
model = ets(imps, model = "ZAA")
prediction = forecast(model, h = 7 * 6)

#this last assignment of varibale is neccessary. It should be the last line.
OUTPUT = list("prediction" = prediction)
